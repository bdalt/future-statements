# Available Programs

## [`analysis/`](analysis/)

## [`evaluation/`](evaluation/)

## [`future-pipeline/`](future-pipeline/)

This directory contains the code run our Pipelines. The code stems from the [WARC-DL repository](https://github.com/webis-de/WARC-DL) in general.

## [`ml/`](ml/)

This directory contains code to train our models.

## [`scrapers/`](scrapers/)

This directory contains code to crawl datasets from [Futurist Speaker](https://futuristspeaker.com/) and the [Early 90s Prediction Database](https://www.elon.edu/u/imagining/time-capsule/early-90s/database-details/). We just used those datasets to evaluate models trained on that data compared to models trained on the webarchive data we extracted via one of our pipelines.
