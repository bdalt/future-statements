# Futurist Speaker

The crawler for data from [Futurist Speaker](https://futuristspeaker.com/) is a scrapy crawler. It is necessary to install scrapy to start this crawler. Then it can be run from within [futurist_speaker](futurist_speaker/) via

```bash
scrapy crawl futurist_speaker
```

# Early 90s Prediction Database

To crawl data from the [Early 90s Prediction Database](https://www.elon.edu/u/imagining/time-capsule/early-90s/90s-database/) run the following command:

```bash
python3 internet_future_db.py
```
